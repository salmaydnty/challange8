const request = require("supertest");
const app = require("../../../../app");

describe("PUT /v1/cars/:id", () => {
  it("should response with 201 as status code", async () => {
    return request(app)
      .put("/v1/cars/:id")
      .then((res) => {
        expect(res.status).not.toBe(200);
        expect(res.body).not.toBe(
          expect.objectContaining({
            cars: expect.arrayContaining([expect.any(Object)]),
            meta: expect.objectContaining({
              pagination: expect.any(Object),
            }),
          })
        );
      });
  });
});
