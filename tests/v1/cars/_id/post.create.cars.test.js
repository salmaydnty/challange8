const request = require("supertest");
const app = require("../../../../app");

const name = "sedan 12";
const price = 0;
const size = "string";
const image = "string";

describe("POST /v1/create", () => {
  let car;

  it("should response with 201 as status code", async () => {
    const accessToken = await request(app).post("/v1/auth/login").send({
      email: "salma@gmail.com",
      password: "123",
    });

    return request(app)
      .post("/v1/cars")
      .set("Content-Type", "application/json")
      .set("Authorization", `Bearer ${accessToken.body.accessToken}`)
      .send({ name, price, size, image })
      .then((res) => {
        expect(res.status).not.toBe(201);
        expect(res.body).not.toBe({
          id: expect.any(Number),
          name: expect.any(String),
          price: expect.any(Number),
          size: expect.any(String),
          image: expect.any(String),
          isCurrentlyRented: expect.any(Boolean),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
        car = res.body;
      });
  });

  it("should response with 401 as status code", async () => {
    const accessToken = await request(app).post("/v1/auth/login").send({
      email: "bukanadmin@gmail.com",
      password: "123",
    });

    return request(app)
      .post("/v1/cars")
      .set("Content-Type", "application/json")
      .set("Authorization", `Bearer ${accessToken.body.accessToken}`)
      .send({ name, price, size, image })
      .then((res) => {
        expect(res.status).toBe(401);
        if (res.body.details === null) {
          expect(res.body).toEqual({
            error: expect.objectContaining({
              name: expect.any(String),
              message: expect.any(String),
              details: null,
            }),
          });
          return;
        }
        expect(res.body).not.toBe({
          error: expect.objectContaining({
            name: expect.any(String),
            message: expect.any(String),
            details: expect.objectContaining({
              role: expect.any(String),
              reason: expect.any(String),
            }),
          }),
        });
      });
  });

  afterAll(async () => {
    const accessToken = await request(app).post("/v1/auth/login").send({
      email: "salma@gmail.com",
      password: "123",
    });

    return request(app)
      .delete("/v1/cars/" + car.id)
      .set("Content-Type", "application/json")
      .set("Authorization", `Bearer ${accessToken.body.accessToken}`);
  });
});
